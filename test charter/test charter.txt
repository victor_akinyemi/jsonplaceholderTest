             Exploratory Testing Charter
			 ============================

Exploratory Testing Charter Purpose:
Goal is to test error handling of the whole JSONPlaceholder REST API

Testers:
Victor Akinyemi and Anton Bob

Date: 
28.01.2020

Timebox or Duration:
1 hour

Target or Scope: 
In this session, only the error handling of the whole JSONPlaceholder API should be tested.
Based on the OWASP security cheat sheet, error handling of the API should only be tested. 
Explicitly check if the API responds with generic error messages without unnecessarily showing technical details of failure to the client

References or Files:
Documentation of the JSONPlaceholder API
https://github.com/OWASP/CheatSheetSeries/blob/master/cheatsheets/REST_Security_Cheat_Sheet.md

Tester Tactics or Tester Charter: 
The testers should both execute positive and negative tests


Test Step Documentation:
Test steps are documented in test rail. https://jsonplaceholdertests.testrail.io/
Credentials to access to the test rail will be sent via mail to respective testers.

Issues Flagged: 
current issues can be seen under the actual result of failed tests in test rail

Test Notes: 
stack trace is displayed to the client when trying to update an unavailable resource with 500 internal server error
stack trace is displayed to the client when trying to create a new post resource with an invalid json request body

Conclusion: two tests failed and needs to be fixed