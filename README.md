#### Please check the acceptance criteria and test case definition in the test design folder.

##### To execute the Build and run all the 5 test scenarios, run the following maven command*
- mvn clean verify serenity:aggregate

##### To execute Tagged tests (T1,T3, T4 and T5), provide the goal as shown in the below format
- mvn clean verify -Dcucumber.options="--tags @T1" serenity:aggregate

After running tests, well detailed report about the serenity bdd execution report could be found under: */target/site/serenity/index.html*

### However, before running the tests or building the project, there is an execution report file in the report folder

######## under the test charter folder, you will find the test charter for teh exploratory test session of to check error handling of the whole JSONPlaceholder REST API
 
##### Tools and Framework used:

- Java 8
- Maven
- Cucumber
- Serenity-BDD
- Lombok
- Rest-assured
- Test rail -> Test management tool



