Feature: verifyPostsResourceNameChange.feature
  As a provider of the JSONPlaceholder REST API
  I want to make sure that all blog posts are handled only by the /posts resource of the JSONPlaceholder REST API
  So that the API design change is not violated

  @T5
  Scenario: T5_getAllAvailableBlogPosts_Via_/Post_Resource_ResourceIsNotFoundAndResponseCodeIs404
    When consumer sends a GET request to the old posts list endpoint
    Then http response code is 404