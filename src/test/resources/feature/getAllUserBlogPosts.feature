Feature: getAllAvailableBlogPosts.feature
  As a provider of the JSONPlaceholder REST API
  I want consumers of the JSONPlaceholder REST API to be able to retrieve all 100 available blog posts only via /posts resource of the API
  So that consumers can use the all retrieved blog posts data in their application

  @T1
  Scenario: T1_getAllAvailableBlogPosts_Via_/Posts_Resource_ResponseCodeIs200AndResponseBodyContainsAllHundredBlogPostsWithDetails
    When consumer sends a GET request to the posts list endpoint
    Then http response code is 200
    And response body contains all 100 blog posts