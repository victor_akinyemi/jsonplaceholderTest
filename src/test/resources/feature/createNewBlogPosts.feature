Feature: createANewBlogPost.feature
  As a provider of the JSONPlaceholder REST API
  I want to be able to create a new blog post via /posts resource of the JSONPlaceholder REST API
  So that consumers can create new blog posts when required

  @T3
  Scenario: T3_createANewBlogPost_Via_/Posts_Resource_ResponseCodeIs201_NewBlogPostIsCreatedWithNewPostId
    When consumer sends a POST request to the posts list endpoint to create a new blog post
    Then http response code is 201
    And response body contains new blog post id and content


