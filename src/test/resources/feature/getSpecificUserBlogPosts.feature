Feature: getSpecificUserBlogPosts.feature
  As a provider of the JSONPlaceholder REST API
  I want consumers of the JSONPlaceholder REST API to be able to retrieve only blog posts of users with valid user ids only via /posts resource of the API
  So that consumers can show only specific blog post data if needed in their application

  @T1
  Scenario Outline: T1_getAllBlogPostsOfAUserWithValidUserId_Via_/Posts_Resource_ResponseCodeIs200AndResponseBodyContainsOnlyBlogPostsWithTheValidUserId
    When consumer sends a GET request to retrieve only blog posts for user with <validUserId>
    Then http response code is 200
    And response body contains only blogs posts with <validUserId>

    Examples:
      | validUserId |
      | 1           |
      | 2           |
      | 3           |
      | 4           |
      | 5           |
      | 6           |
      | 7           |
      | 8           |
      | 9           |
      | 10          |


  @T4
  Scenario Outline: T4_getBlogPostsOfAUserWithAnInvalidUserId_Via_/Posts_Resource_ResponseCodeIs200AndResponseBodyIsEmpty
    When consumer sends a GET request to retrieve blog posts with an <invalidUserId>
    Then http response code is 200
    And response body is an empty array

    Examples:
      | invalidUserId |
      | blabla        |
      | jjja          |

