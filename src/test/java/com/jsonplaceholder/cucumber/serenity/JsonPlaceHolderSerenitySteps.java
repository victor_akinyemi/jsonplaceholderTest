package com.jsonplaceholder.cucumber.serenity;

import com.jsonplaceholder.model.Post;
import com.jsonplaceholder.utils.ReuseableSpecifications;
import io.restassured.response.ValidatableResponse;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

public class JsonPlaceHolderSerenitySteps {

    @Step("Get all blog posts")
    public ValidatableResponse getAllBlogPosts() {
        ValidatableResponse response = SerenityRest.rest().given()
                .spec(ReuseableSpecifications.getGenericRequestSpec())
                .when()
                .get("/posts")
                .then();
        Serenity.setSessionVariable("requestResponse").to(response);
        return response;

    }

    @Step("create new blog post with userId: {}, title:{}, body: {}")
    public ValidatableResponse createNewBlogPost(int userId, String title, String body) {
        Post post = new Post();
        post.setBody(body);
        post.setTitle(title);
        post.setUserId(userId);
        Serenity.setSessionVariable("newBlogPost").to(post);

        ValidatableResponse response = SerenityRest.rest().given()
                .spec(ReuseableSpecifications.getGenericRequestSpec())
                .when()
                .body(post)
                .post("/posts")
                .then();

        Serenity.setSessionVariable("requestResponse").to(response);
        return response;

    }

    @Step("Get all blog posts from old endpoint")
    public ValidatableResponse getBlogPostsViaOldResourceName() {
        ValidatableResponse response = SerenityRest.rest().given()
                .spec(ReuseableSpecifications.getGenericRequestSpec())
                .when()
                .get("/post")
                .then();
        Serenity.setSessionVariable("requestResponse").to(response);
        return response;

    }

    @Step("Get blog posts for specific user id")
    public ValidatableResponse getBlogPostsForSpecificUserId(int userId) {
        ValidatableResponse response = SerenityRest.rest().given()
                .spec(ReuseableSpecifications.getGenericRequestSpec())
                .queryParam("userId", userId)
                .when()
                .get("/posts")
                .then();
        Serenity.setSessionVariable("requestResponse").to(response);
        return response;

    }

    @Step("Get blog posts for invalid user id")
    public ValidatableResponse getBlogPostsForInvalidUserId(String userId) {
        ValidatableResponse response = SerenityRest.rest().given()
                .spec(ReuseableSpecifications.getGenericRequestSpec())
                .queryParam("userId", userId)
                .when()
                .get("/posts")
                .then();
        Serenity.setSessionVariable("requestResponse").to(response);
        return response;

    }
}
