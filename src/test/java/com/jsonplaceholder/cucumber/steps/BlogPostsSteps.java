package com.jsonplaceholder.cucumber.steps;

import com.jsonplaceholder.cucumber.serenity.JsonPlaceHolderSerenitySteps;
import com.jsonplaceholder.model.Post;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.response.ValidatableResponse;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Steps;

import static com.jayway.jsonpath.matchers.JsonPathMatchers.hasJsonPath;
import static org.hamcrest.Matchers.*;

public class BlogPostsSteps {

    @Steps
    private JsonPlaceHolderSerenitySteps steps;

    @When("^consumer sends a GET request to the posts list endpoint$")
    public void sendRequestToGetAllPosts() {
        steps.getAllBlogPosts();
    }

    @When("^consumer sends a GET request to retrieve only blog posts for user with (.*)$")
    public void sendGetRequestToPostsEndpoint(int userId) {
        steps.getBlogPostsForSpecificUserId(userId);
    }

    @When("^consumer sends a GET request to retrieve blog posts with an (.*)$")
    public void sendGetRequestToPostsEndpoint(String userId) {
        steps.getBlogPostsForInvalidUserId(userId);
    }

    @When("^consumer sends a POST request to the posts list endpoint to create a new blog post$")
    public void createNewBlogPosts() {
        steps.createNewBlogPost(11, "new blog post title", "new blog post body");
    }

    @Then("^http response code is (.*)$")
    public void verifyResponseCode(int code) {
        ValidatableResponse response = Serenity.sessionVariableCalled("requestResponse");
        response.assertThat().statusCode(code);
    }

    @Then("^response body contains new blog post id and content$")
    public void verifyNewlyCreatedBlogPost() {
        Post expectedPost = Serenity.sessionVariableCalled("newBlogPost");
        ValidatableResponse response = Serenity.sessionVariableCalled("requestResponse");
        response.body("$", hasJsonPath("id", is(101)));
        response.body("$", hasJsonPath("userId", is(expectedPost.getUserId())));
        response.body("$", hasJsonPath("title", is(expectedPost.getTitle())));
        response.body("$", hasJsonPath("body", is(expectedPost.getBody())));
    }

    @When("^consumer sends a GET request to the old posts list endpoint$")
    public void sendGetRequestToOldPostsEndpoint() {
        steps.getBlogPostsViaOldResourceName();
    }

    @Then("^response body contains all (.*) blog posts$")
    public void verifyResponseAllBlogPostResponseBody(int number) {
        ValidatableResponse response = Serenity.sessionVariableCalled("requestResponse");
        response.body("size()", is(number));
        response.body("", allOf(hasItem(hasJsonPath("userId")), hasItem(hasJsonPath("id")), hasItem(hasJsonPath("title")), hasItem(hasJsonPath("body"))));
    }

    @Then("^response body contains only blogs posts with (.*)$")
    public void verifyBlogPostForSpecificUserId(int userId) {
        ValidatableResponse response = Serenity.sessionVariableCalled("requestResponse");
        response.body("", allOf(hasItem(hasJsonPath("userId", is(userId)))));
    }

    @Then("^response body is an empty array$")
    public void verifyThatResponseBodyIsAnEmptyArray() {
        ValidatableResponse response = Serenity.sessionVariableCalled("requestResponse");
        response.body("", empty());
    }
}
